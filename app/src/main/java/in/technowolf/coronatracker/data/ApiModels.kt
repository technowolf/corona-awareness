/*
 * MIT License
 *
 * Copyright (c) 2020 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package `in`.technowolf.coronatracker.data

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class AffectedCountriesRS(
    @SerializedName("affected_countries")
    val affectedCountries: List<String> = listOf(),
    @SerializedName("statistic_taken_at")
    val statisticTakenAt: String = ""
)

@Keep
data class CasesByCountryRS(
    @SerializedName("countries_stat")
    val countriesStat: List<CountriesStat> = listOf(),
    @SerializedName("statistic_taken_at")
    val statisticTakenAt: String = ""
) {
    @Keep
    data class CountriesStat(
        @SerializedName("active_cases")
        val activeCases: String = "",
        @SerializedName("cases")
        val cases: String = "",
        @SerializedName("country_name")
        val countryName: String = "",
        @SerializedName("deaths")
        val deaths: String = "",
        @SerializedName("new_cases")
        val newCases: String = "",
        @SerializedName("new_deaths")
        val newDeaths: String = "",
        @SerializedName("region")
        val region: String = "",
        @SerializedName("serious_critical")
        val seriousCritical: String = "",
        @SerializedName("total_cases_per_1m_population")
        val totalCasesPer1mPopulation: String = "",
        @SerializedName("total_recovered")
        val totalRecovered: String = ""
    )
}