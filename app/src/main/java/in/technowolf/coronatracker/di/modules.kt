/*
 * MIT License
 *
 * Copyright (c) 2020 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package `in`.technowolf.coronatracker.di

import `in`.technowolf.coronatracker.BuildConfig
import `in`.technowolf.coronatracker.data.AffectedCountriesService
import `in`.technowolf.coronatracker.ui.affectedCountries.AffectedCountriesViewModel
import com.chuckerteam.chucker.api.ChuckerCollector
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.chuckerteam.chucker.api.RetentionManager
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException

val retrofitModule = module {

    single { okHttpProvider(get()) }
    single { retrofit(BuildConfig.baseUrl, get()) }

}

val repoModule = module {
    single {
        get<Retrofit>().create(AffectedCountriesService::class.java)
    }
}

val viewModelModule = module {
    viewModel {
        AffectedCountriesViewModel(get())
    }
}

val appModule = module {
    single {
        ChuckerCollector(
            context = get(),
            showNotification = true,
            retentionPeriod = RetentionManager.Period.ONE_HOUR
        )
    }
    single {
        ChuckerInterceptor(
            context = get(),
            collector = get(),
            maxContentLength = 250000L,
            headersToRedact = setOf("x-rapidapi-key")
        )
    }
}

private fun okHttpProvider(chuckerInterceptor: ChuckerInterceptor): OkHttpClient {
    val okHttpClient = OkHttpClient.Builder()
    okHttpClient.addInterceptor(object : Interceptor {
            @Throws(IOException::class)
            override fun intercept(chain: Interceptor.Chain): Response {
                val request: Request =
                    chain.request().newBuilder()
                        .addHeader("x-rapidapi-host", BuildConfig.headerHost)
                        .addHeader("x-rapidapi-key", BuildConfig.apiKey)
                        .build()
                return chain.proceed(request)
            }
        })
        .addInterceptor(chuckerInterceptor)

    return okHttpClient.build()
}


@Suppress("SameParameterValue")
private fun retrofit(baseUrl: String, okHttpClient: OkHttpClient) = Retrofit.Builder()
    .baseUrl(baseUrl)
    .client(okHttpClient)
    .addConverterFactory(GsonConverterFactory.create())
    .build()
